<?php

class Animal
{
  public $name;
  public $legs = 4;
  public $berdarah_dingin = "no";

  public function __construct($sheep)
  {
    $this->name = $sheep;
  }
}

// $sheep = new Animal("shaun");

// echo $sheep->name; // "shaun"
// echo $sheep->legs; // 4
// echo $sheep->berdarah_dingin; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())