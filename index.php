<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');


$animal = new Animal("shaun");
echo "Nama Hewan: " . $animal->name . "<br>";
echo "Jumlah Kaki: " . $animal->legs . "<br>";
echo "Berdarah dingin: " . $animal->berdarah_dingin . "<br> <br>";

$frog = new Frog("buduk");
echo "Nama Hewan: " . $frog->name . "<br>";
echo "Jumlah Kaki: " . $frog->legs . "<br>";
echo "Berdarah dingin: " . $frog->berdarah_dingin . "<br>";
echo "Jump : ";
echo $frog->jump() . "<br> <br>";

$sungokong = new Ape("kera sakti");
echo "Nama Hewan: " . $sungokong->name . "<br>";
echo "Jumlah Kaki: " . $sungokong->legs . "<br>";
echo "Berdarah dingin: " . $sungokong->berdarah_dingin . "<br>";
echo "Yell : ";
echo $sungokong->yell();
// echo "Yel; "<br>";
// ech ll: " .  . "<br>";
// $sungokong = new Ape("kera sakti");
// $sungokong->yell() // "Auooo"

// $kodok = new Frog("buduk");
// $kodok->jump() ; // "hop hop"